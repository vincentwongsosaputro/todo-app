import React from 'react';
import { ApolloConsumer } from 'react-apollo';
import { AsyncStorage } from 'react-native';

import Intro from './components/Intro';
import TodoList from './components/TodoList';
import TodoListDetail from './components/TodoListDetail';

export default class Main extends React.Component {
  state = {
    firstTime: !this.props.userId,
    userId: this.props.userId,
    selectedTodoList: null,
  };

  saveUser = id => {
    AsyncStorage.setItem('@store:userId', id);
    this.setState({
      firstTime: false,
      userId: id,
    });
  };

  selectTodoList = selectedTodoList => {
    this.setState({
      selectedTodoList,
    });
  };

  backToList = () => {
    this.setState({
      selectedTodoList: null,
    });
  };

  render() {
    const { firstTime, userId, selectedTodoList } = this.state;
    if (firstTime) {
      return (
        <ApolloConsumer>
          {client => <Intro testID="intro-component" client={client} saveUser={this.saveUser} />}
        </ApolloConsumer>
      );
    }
    if (selectedTodoList) {
      return (
        <ApolloConsumer>
          {client => (
            <TodoListDetail
              testID="todo-list-detail-component"
              selectedTodoList={selectedTodoList}
              client={client}
              backToList={this.backToList}
            />
          )}
        </ApolloConsumer>
      );
    }
    return (
      <ApolloConsumer>
        {client => (
          <TodoList
            testID="todo-list-component"
            userId={userId}
            client={client}
            selectTodoList={this.selectTodoList}
          />
        )}
      </ApolloConsumer>
    );
  }
}
