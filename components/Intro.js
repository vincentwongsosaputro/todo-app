import React, { Component } from 'react';
import {
  StyleSheet, Text, Dimensions, View
} from 'react-native';
import { Constants } from 'expo';
import {
  Container, Content, Item, Input, Button
} from 'native-base';
import gql from 'graphql-tag';

const CREATE_USER = gql`
  mutation CreateUser($email: String!, $deviceID: String!) {
    createUser(email: $email, deviceID: $deviceID) {
      id
      email
      deviceID
      createdAt
    }
  }
`;

const GET_USER = gql`
  query User($email: String!) {
    User(email: $email) {
      id
      email
      deviceID
    }
  }
`;

const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    flex: 1,
    justifyContent: 'center',
  },
  welcome: {
    flex: 1,
    marginTop: deviceHeight / 8,
    marginBottom: 30,
    alignItems: 'center',
  },
  startButton: {
    alignItems: 'center',
    margin: 15,
  },
});

class Intro extends Component {
  state = {
    email: '',
  };

  getUser = async () => {
    const { email } = this.state;
    const { client } = this.props;

    const result = await client.query({
      query: GET_USER,
      variables: {
        email,
      },
    });
    return result.data.User;
  };

  createUser = async () => {
    const { email } = this.state;
    const { client } = this.props;

    const result = await client.mutate({
      mutation: CREATE_USER,
      variables: {
        email,
        deviceID: Constants.deviceId,
      },
    });
    return result.data.createUser;
  };

  handleChange = text => this.setState({ email: text });

  handleSubmit = async () => {
    const { saveUser } = this.props;
    let user = await this.getUser();
    if (!user) {
      user = await this.createUser();
      this.setState({ email: '' });
    }
    saveUser(user.id);
  };

  render() {
    const { email } = this.state;

    return (
      <Container style={styles.container}>
        <Content>
          <View style={styles.welcome}>
            <Text style={styles.welcome} testID="welcome-message">
              Welcome to To Do List
            </Text>
          </View>
          <Text style={styles.instructions}>To get started please enter your email</Text>
          <Item underline>
            <Input
              style={{ textAlign: 'center' }}
              testID="email-text"
              value={email}
              onChangeText={this.handleChange}
            />
          </Item>
          <Button
            block
            primary
            style={styles.startButton}
            success
            testID="start-button"
            onPress={this.handleSubmit}
          >
            <Text style={{ color: 'white' }}>Let's Get Started</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default Intro;
