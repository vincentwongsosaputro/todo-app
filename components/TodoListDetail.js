import React, { Component } from 'react';
import { Text } from 'react-native';
import {
  Container,
  Header,
  Content,
  Body,
  Title,
  List,
  ListItem,
  Button,
  Icon,
  Item,
  Input,
  Right,
  Left,
  CheckBox,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

import gql from 'graphql-tag';

const GET_TODO_LIST_ITEM = gql`
  query ToDoListItem($todoListId: ID!) {
    ToDoList(id: $todoListId) {
      toDoListItems {
        id
        name
        done
        createdAt
      }
    }
  }
`;

const CREATE_TODO_LIST_ITEM = gql`
  mutation CreateToDoListItem($name: String!, $todoListId: ID!) {
    createToDoListItem(name: $name, toDoListId: $todoListId, done: false) {
      id
      name
      done
      createdAt
    }
  }
`;

const DELETE_TODO_LIST_ITEM = gql`
  mutation DeleteToDoListItem($id: ID!) {
    deleteToDoListItem(id: $id) {
      id
    }
  }
`;

const UPDATE_TODO_LIST_ITEM = gql`
  mutation UpdateToDoListItem($id: ID!, $done: Boolean!) {
    updateToDoListItem(id: $id, done: $done) {
      id
      name
      done
      createdAt
    }
  }
`;

export default class TodoListDetail extends Component {
  state = {
    todoListItems: [],
    itemName: '',
  };

  async componentDidMount() {
    const { selectedTodoList, client } = this.props;
    const result = await client.query({
      query: GET_TODO_LIST_ITEM,
      variables: {
        todoListId: selectedTodoList.id,
      },
      fetchPolicy: 'no-cache',
    });
    const todoListItems = result.data.ToDoList.toDoListItems.map(item => ({
      id: item.id,
      name: item.name,
      done: item.done,
    }));
    this.setState({ todoListItems });
  }

  handleChange = text => this.setState({
    itemName: text,
  });

  addNewTodoItem = async () => {
    const { itemName } = this.state;
    const { client, selectedTodoList } = this.props;
    const todoListItems = [...this.state.todoListItems];

    if (itemName !== '') {
      const result = await client.mutate({
        mutation: CREATE_TODO_LIST_ITEM,
        variables: {
          name: itemName,
          todoListId: selectedTodoList.id,
        },
      });
      if (result) {
        todoListItems.push({
          id: result.data.createToDoListItem.id,
          name: itemName,
          done: result.data.createToDoListItem.done,
        });
      }
      this.setState({ todoListItems, itemName: '' });
    }
  };

  removeItem = async id => {
    const { client } = this.props;
    const result = await client.mutate({
      mutation: DELETE_TODO_LIST_ITEM,
      variables: {
        id,
      },
    });
    if (result) {
      const todoListItems = [...this.state.todoListItems];
      const index = todoListItems.findIndex(item => item.id === id);
      todoListItems.splice(index, 1);
      this.setState({ todoListItems });
    }
  };

  updateToDoItem = async id => {
    const { client } = this.props;
    const todoListItems = [...this.state.todoListItems];
    const index = todoListItems.findIndex(item => item.id === id);

    const result = await client.mutate({
      mutation: UPDATE_TODO_LIST_ITEM,
      variables: {
        id,
        done: !todoListItems[index].done,
      },
    });
    if (result) {
      todoListItems[index].done = !todoListItems[index].done;
      this.setState({ todoListItems });
    }
  };

  render() {
    const { selectedTodoList, backToList } = this.props;
    const { todoListItems, itemName } = this.state;
    return (
      <Container>
        <Header>
          <Left>
            <Button testID="back-button" transparent onPress={backToList}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>
              <Text testID="todo-list-title">{selectedTodoList.name}</Text>
            </Title>
          </Body>
        </Header>
        <Content>
          <Grid>
            <Row>
              <Col>
                {todoListItems.length === 0 && (
                  <Text style={{ textAlign: 'center', marginTop: 15 }} testID="no-item-message">
                    You have no to do item, please create one
                  </Text>
                )}
                {todoListItems.length > 0 && (
                  <List testID="todo-list-items">
                    {todoListItems.map(item => (
                      <ListItem testID="todo-list-item" key={item.id}>
                        <CheckBox
                          onPress={() => this.updateToDoItem(item.id)}
                          testID="todo-list-item-checkbox"
                          checked={item.done}
                          color="green"
                        />
                        <Body style={{ marginLeft: 10 }}>
                          <Text testID="todo-list-item-name">{item.name}</Text>
                        </Body>
                        <Right>
                          <Icon
                            testID="todo-list-item-remove-button"
                            onPress={() => this.removeItem(item.id)}
                            name="trash"
                          />
                        </Right>
                      </ListItem>
                    ))}
                  </List>
                )}
              </Col>
            </Row>
            <Row>
              <Col style={{ marginTop: 10 }}>
                <Item underline>
                  <Input
                    testID="new-item-text"
                    placeholder="Todo Item Name"
                    value={itemName}
                    onChangeText={this.handleChange}
                  />
                </Item>
                <Button testID="add-new-item-button" block onPress={this.addNewTodoItem}>
                  <Text style={{ color: 'white' }}>Add New Todo Item</Text>
                </Button>
              </Col>
            </Row>
          </Grid>
        </Content>
      </Container>
    );
  }
}
