import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Container,
  Header,
  Content,
  Body,
  Title,
  List,
  ListItem,
  Text,
  Button,
  Icon,
  Item,
  Input,
  Right,
  Left,
} from 'native-base';

import { Col, Row, Grid } from 'react-native-easy-grid';

import gql from 'graphql-tag';

const GET_TODO_LIST = gql`
  query TodoList($userId: ID!) {
    User(id: $userId) {
      toDoLists {
        id
        name
      }
    }
  }
`;

const CREATE_TODO_LIST = gql`
  mutation CreateToDoList($name: String!, $userId: ID!) {
    createToDoList(name: $name, userId: $userId) {
      id
      name
      createdAt
    }
  }
`;

const DELETE_TODO_LIST = gql`
  mutation DeleteToDoList($id: ID!) {
    deleteToDoList(id: $id) {
      id
    }
  }
`;

export default class TodoList extends Component {
  state = {
    todoList: [],
    listName: '',
  };

  async componentDidMount() {
    const { userId, client } = this.props;
    const result = await client.query({
      query: GET_TODO_LIST,
      variables: {
        userId,
      },
      fetchPolicy: 'no-cache',
    });

    const todoList = result.data.User.toDoLists.map(item => ({ id: item.id, name: item.name }));
    this.setState({ todoList });
  }

  handleChange = text => {
    this.setState({
      listName: text,
    });
  };

  addNewList = async () => {
    const { client, userId } = this.props;
    const { listName } = this.state;
    if (listName !== '') {
      const result = await client.mutate({
        mutation: CREATE_TODO_LIST,
        variables: {
          name: listName,
          userId,
        },
      });
      if (result) {
        const listId = result.data.createToDoList.id;
        const todoList = [...this.state.todoList];
        todoList.push({
          id: listId,
          name: listName,
        });
        this.setState({
          todoList,
          listName: '',
        });
      }
    }
  };

  removeList = async id => {
    const { client } = this.props;
    const result = await client.mutate({
      mutation: DELETE_TODO_LIST,
      variables: {
        id,
      },
    });
    if (result) {
      const todoList = [...this.state.todoList];
      const index = todoList.findIndex(item => item.id === id);
      todoList.splice(index, 1);
      this.setState({ todoList });
    }
  };

  selectTodo = id => {
    const { selectTodoList } = this.props;
    const { todoList } = this.state;
    const selectedTodoList = todoList.find(item => item.id === id);
    selectTodoList(selectedTodoList);
  };

  render() {
    const { todoList, listName } = this.state;
    return (
      <Container>
        <Header>
          <Body>
            <Title>To Do List</Title>
          </Body>
        </Header>
        <Content>
          <Grid>
            <Row size={80}>
              <Col>
                {todoList.length === 0 && (
                  <Text testID="no-list-message">You have no to do list, please create one</Text>
                )}
                {todoList.length > 0 && (
                  <List testID="todo-list">
                    {todoList.map(item => (
                      <ListItem
                        icon
                        key={item.id}
                        onPress={() => this.selectTodo(item.id)}
                        testID="todo-list-selector"
                      >
                        <Left>
                          <Button testID="remove-button" onPress={() => this.removeList(item.id)}>
                            <Icon active name="trash" />
                          </Button>
                        </Left>
                        <Body>
                          <Text>{item.name}</Text>
                        </Body>
                        <Right>
                          <Icon name="arrow-forward" />
                        </Right>
                      </ListItem>
                    ))}
                  </List>
                )}
              </Col>
            </Row>
            <Row size={20}>
              <Col>
                <Item style={{ marginTop: 10 }}>
                  <Input
                    testID="new-list-text"
                    placeholder="List Name"
                    value={listName}
                    onChangeText={this.handleChange}
                  />
                </Item>
                <Button testID="add-new-list-button" block onPress={this.addNewList}>
                  <Text>Add New List</Text>
                </Button>
              </Col>
            </Row>
          </Grid>
        </Content>
      </Container>
    );
  }
}
