import React from 'react';
import { Font, AppLoading } from 'expo';
import { AsyncStorage } from 'react-native';
import { ApolloProvider } from 'react-apollo';
import ApolloClient from 'apollo-boost';
import Main from './Main';

// replace uri below with the endpoint from the previous step
const client = new ApolloClient({
  uri: 'https://api.graph.cool/simple/v1/cjlep525s38hl0168t3v4lwxg',
});

export default class App extends React.Component {
  state = {
    userId: null,
    isReady: false,
  };

  loadUserData = async () => {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
    });

    const userId = await AsyncStorage.getItem('@store:userId');
    this.setState({
      userId,
    });
  };

  render() {
    const { userId, isReady } = this.state;
    if (!isReady) {
      return (
        <AppLoading
          startAsync={this.loadUserData}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }
    return (
      <ApolloProvider client={client}>
        <Main userId={userId} />
      </ApolloProvider>
    );
  }
}
