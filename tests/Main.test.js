import React from 'react';
import renderer from 'react-test-renderer';
import wait from 'waait';
import { AsyncStorage } from 'react-native';

import { MockedProvider } from 'react-apollo/test-utils';

import Main from '../Main';

jest.mock('react-native');

const setup = async (props = {}, state = null) => {
  const testRenderer = renderer.create(
    <MockedProvider mocks={[]} removeTypename>
      <Main testID="main-component" {...props} />
    </MockedProvider>,
  );
  await wait(0); // wait for response
  const mainComponent = testRenderer.root.findByProps({ testID: 'main-component' });
  const mainComponentInstance = mainComponent.instance;
  if (state) {
    mainComponentInstance.setState(state);
  }
  return mainComponent;
};

test('render intro for first time access', async () => {
  const firstTime = true;
  const component = await setup({}, { firstTime });
  component.findByProps({ testID: 'intro-component' });
});

test('do not render intro if it is not first time access', async () => {
  const firstTime = false;
  const component = await setup({}, { firstTime });
  expect(() => component.findByProps({ testID: 'intro-component' })).toThrow();
});

test('render todo list if it is not first time access', async () => {
  const firstTime = false;
  const component = await setup({}, { firstTime });
  component.findByProps({ testID: 'todo-list-component' });
});

test('when saveUser is being called it should update user id state', async () => {
  const firstTime = false;
  const userId = '1111111';
  const component = await setup({ userId: null }, { firstTime });
  await component.instance.saveUser(userId);
  expect(component.instance.state.userId).toEqual(userId);
});

test('render todo list detail if one of the list is selected', async () => {
  const firstTime = false;
  const selectedTodoList = { id: '1111', name: 'my todo list' };
  const component = await setup({}, { firstTime, selectedTodoList });
  component.findByProps({ testID: 'todo-list-detail-component' });
});
