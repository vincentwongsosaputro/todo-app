import React from 'react';
import { shallow } from 'enzyme';
import wait from 'waait';

import { findByTestID } from '../test-utils';
import Intro from '../../components/Intro';

/**
 * Function to create a ShallowWrapper for the Main component
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @param {object} state
 * @returns {ShallowWrapper}
 */
const setup = (props = {}, state = null) => {
  const wrapper = shallow(<Intro {...props} />);
  if (state) {
    wrapper.setState(state);
  }
  return wrapper;
};

test('render welcome message', () => {
  const wrapper = setup();
  const welcomeMessage = findByTestID(wrapper, 'welcome-message');
  expect(welcomeMessage.length).toEqual(1);
});
test('render email text box', () => {
  const wrapper = setup();

  const emailText = findByTestID(wrapper, 'email-text');
  expect(emailText.length).toEqual(1);
});

test('render start button', () => {
  const wrapper = setup();

  const startButton = findByTestID(wrapper, 'start-button');
  expect(startButton.length).toEqual(1);
});

test('email textbox on change update local state', () => {
  const wrapper = setup();

  const email = 'test@test.com';
  const emailText = findByTestID(wrapper, 'email-text');
  emailText.simulate('changeText', email);
  expect(wrapper.state('email')).toEqual(email);
});

test('should only create user if user does not exist', async () => {
  const email = 'notexist@email.com';
  const client = {
    mutate: jest.fn(() => Promise.resolve({})),
    query: jest.fn(() => Promise.resolve({
      data: {
        User: null,
      },
    }), ),
  };

  const wrapper = setup({ client }, null);
  const emailText = findByTestID(wrapper, 'email-text');
  emailText.simulate('changeText', email);

  const startButton = findByTestID(wrapper, 'start-button');
  startButton.simulate('press');
  await wait(0);
  expect(client.query).toHaveBeenCalledTimes(1);
  expect(client.mutate).toHaveBeenCalledTimes(1);
});

test('should not create user if user already exist in database', async () => {
  const email = 'test@test.com';
  const client = {
    mutate: jest.fn(() => Promise.resolve({})),
    query: jest.fn(() => Promise.resolve({
      data: {
        User: {
          email,
          id: '11111111',
        },
      },
    }), ),
  };

  const wrapper = setup({ client }, null);
  const emailText = findByTestID(wrapper, 'email-text');
  emailText.simulate('changeText', email);

  const startButton = findByTestID(wrapper, 'start-button');
  startButton.simulate('press');
  await wait(0);
  expect(client.query).toHaveBeenCalledTimes(1);
  expect(client.mutate).toHaveBeenCalledTimes(0);
});

test('should call save user to save user to localstorage', async () => {
  const email = 'test@email.com';
  const saveUser = jest.fn();
  const userId = '11111';
  const client = {
    mutate: jest.fn(() => Promise.resolve({
      data: {
        createUser: {
          id: userId
        },
      },
    })),
    query: jest.fn(() => Promise.resolve({
      data: {
        User: null,
      },
    }), ),
  };
  const wrapper = setup({ client, saveUser }, { email });
  const startButton = findByTestID(wrapper, 'start-button');
  startButton.simulate('press');
  await wait(0);
  expect(saveUser).toHaveBeenCalledTimes(1);
  expect(saveUser).toHaveBeenCalledWith(userId);
});
