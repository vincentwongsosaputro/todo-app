import React from 'react';
import { shallow } from 'enzyme';
import wait from 'waait';

import { findByTestID } from '../test-utils';
import TodoList from '../../components/TodoList';

/**
 * Function to create a ShallowWrapper for the Main component
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @param {object} state
 * @returns {ShallowWrapper}
 */
const setup = (props = {}, state = null) => {
  const wrapper = shallow(<TodoList {...props} />);
  if (state) {
    wrapper.setState(state);
  }
  return wrapper;
};

test('render todo lists if available', () => {
  const todoList = [
    { id: '1', name: 'list 1' },
    { id: '2', name: 'list 2' },
    { id: '3', name: 'list 3' },
  ];
  const wrapper = setup({}, { todoList });
  const todoListComponent = findByTestID(wrapper, 'todo-list');
  expect(todoListComponent.length).toEqual(1);
  expect(todoListComponent.children().length).toEqual(3);
});

test('render no to do list message if there is no list', () => {
  const todoList = [];
  const wrapper = setup({}, { todoList });
  const todoListComponent = findByTestID(wrapper, 'no-list-message');
  expect(todoListComponent.length).toEqual(1);
});

test('should check database for stored todo list when component mounted', async () => {
  const todoList = [];
  const todoListFromDb = {
    id: '1',
    name: 'my list'
  };
  const client = {
    query: jest.fn(() => Promise.resolve({
      data: {
        User: {
          toDoLists: [
            todoListFromDb
          ]
        },
      },
    })),
  };
  const wrapper = setup({ client }, { todoList });
  await wait(0);
  expect(client.query).toHaveBeenCalledTimes(1);
  expect(wrapper.state('todoList')).toEqual([todoListFromDb]);
});

test('render new list textbox', () => {
  const wrapper = setup();
  const newListText = findByTestID(wrapper, 'new-list-text');
  expect(newListText.length).toEqual(1);
});

test('new list textbox on change update local state', () => {
  const wrapper = setup();

  const newListName = 'Shopping List';
  const newListText = findByTestID(wrapper, 'new-list-text');
  newListText.simulate('changeText', newListName);
  expect(wrapper.state('listName')).toEqual(newListName);
});

test('render create new list button', () => {
  const wrapper = setup();
  const addNewListButton = findByTestID(wrapper, 'add-new-list-button');
  expect(addNewListButton.length).toEqual(1);
});

test('creating new list should render the new list', async () => {
  const id = '11111';
  const client = {
    mutate: jest.fn(() => Promise.resolve({
      data: {
        createToDoList: {
          id
        },
      },
    })),
  };

  const newListName = 'Shopping List';
  const todoList = [];
  const wrapper = setup({ client }, { listName: newListName, todoList });

  const addNewListButton = findByTestID(wrapper, 'add-new-list-button');
  addNewListButton.simulate('press');
  await wait(0);
  expect(wrapper.state('todoList')).toEqual([{ id, name: newListName }]);

  wrapper.update();
  const todoListComponent = findByTestID(wrapper, 'todo-list');
  expect(todoListComponent.length).toEqual(1);
});

test('creating new list should mutate the graphql todo list', async () => {
  const client = {
    mutate: jest.fn(() => Promise.resolve({})),
  };
  const newListName = 'Shopping List';
  const todoList = [];
  const wrapper = setup({ client }, { listName: newListName, todoList });

  const addNewListButton = findByTestID(wrapper, 'add-new-list-button');
  addNewListButton.simulate('press');

  await wait(0);
  expect(client.mutate).toHaveBeenCalledTimes(1);
});

test('deleting a list should remove it from database and screen', async () => {
  const client = {
    mutate: jest.fn(() => Promise.resolve({})),
  };
  const listId = '1111';
  const listName = 'list 1';
  const todoList = [{
    id: listId,
    name: listName
  }];
  const wrapper = setup({ client }, { todoList });
  const deleteButton = findByTestID(wrapper, 'remove-button').first();
  expect(deleteButton.length).toEqual(1);
  deleteButton.simulate('press');
  await wait(0);
  expect(client.mutate).toHaveBeenCalledTimes(1);
  expect(wrapper.state('todoList')).toEqual([]);
});

test('clicking one of the list item should select that to do list', () => {
  const todoList = [{
    id: '1111',
    name: 'my list'
  }];
  const selectTodoList = jest.fn();
  const wrapper = setup({ selectTodoList }, { todoList });
  const todoListSelector = findByTestID(wrapper, 'todo-list-selector').first();
  todoListSelector.simulate('press');
  expect(selectTodoList).toHaveBeenCalledTimes(1);
});
