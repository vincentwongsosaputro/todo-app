import React from 'react';
import { shallow } from 'enzyme';
import wait from 'waait';

import { findByTestID } from '../test-utils';
import TodoListDetail from '../../components/TodoListDetail';

/**
 * Function to create a ShallowWrapper for the Main component
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @param {object} state
 * @returns {ShallowWrapper}
 */
const setup = (props = {}, state = null) => {
  const wrapper = shallow(<TodoListDetail {...props} />);
  if (state) {
    wrapper.setState(state);
  }
  return wrapper;
};

test('render todo list title correctly', () => {
  const selectedTodoList = { id: '1', name: 'My List' };
  const wrapper = setup({ selectedTodoList });
  const todoListTitle = findByTestID(wrapper, 'todo-list-title');
  expect(todoListTitle.length).toEqual(1);
  expect(todoListTitle.dive().text()).toEqual('My List');
});

test('render todo list items if available', () => {
  const selectedTodoList = { id: '1', name: 'My List' };
  const todoListItems = [
    { id: '1', name: 'item 1', done: false },
    { id: '2', name: 'item 2', done: false },
    { id: '3', name: 'item 3', done: false },
  ];
  const wrapper = setup({ selectedTodoList }, { todoListItems });
  const todoListItemsComponent = findByTestID(wrapper, 'todo-list-items');
  expect(todoListItemsComponent.length).toEqual(1);
  expect(todoListItemsComponent.children().length).toEqual(3);
});

test('render no to do list item message if there is no list', () => {
  const selectedTodoList = { id: '1', name: 'My List' };
  const wrapper = setup({ selectedTodoList });
  const todoListItemsComponent = findByTestID(wrapper, 'todo-list-items');
  expect(todoListItemsComponent.length).toEqual(0);

  const noMessageText = findByTestID(wrapper, 'no-item-message');
  expect(noMessageText.length).toEqual(1);
});

test('todo list item should render checkbox, name, remove button', () => {
  const selectedTodoList = { id: '1', name: 'My List' };
  const todoListItems = [{ id: '1', name: 'item 1', done: false }];
  const wrapper = setup({ selectedTodoList }, { todoListItems });
  const todoListItemComponent = findByTestID(wrapper, 'todo-list-item').first();
  expect(todoListItemComponent.length).toEqual(1);
  const todoListItemCheckboxComponent = findByTestID(
    todoListItemComponent,
    'todo-list-item-checkbox',
  );
  expect(todoListItemCheckboxComponent.length).toEqual(1);
  const todoListItemNameComponent = findByTestID(todoListItemComponent, 'todo-list-item-name');
  expect(todoListItemNameComponent.length).toEqual(1);
  const todoListItemRemoveComponent = findByTestID(
    todoListItemComponent,
    'todo-list-item-remove-button',
  );
  expect(todoListItemRemoveComponent.length).toEqual(1);
});

test('should go back to todo lists when back button is pressed', () => {
  const selectedTodoList = { id: '1', name: 'My List' };
  const backToList = jest.fn();
  const wrapper = setup({ selectedTodoList, backToList });

  const backButton = findByTestID(wrapper, 'back-button').first();
  backButton.simulate('press');
  expect(backToList).toHaveBeenCalledTimes(1);
});

test('render new todo item textbox and button', () => {
  const selectedTodoList = { id: '1', name: 'My List' };
  const wrapper = setup({ selectedTodoList }, { todoListItems: [] });
  const newTodoItemText = findByTestID(wrapper, 'new-item-text');
  expect(newTodoItemText.length).toEqual(1);
  const addNewTodoItemButton = findByTestID(wrapper, 'add-new-item-button');
  expect(addNewTodoItemButton.length).toEqual(1);
});

test('text changes on new item textbox should update local state', () => {
  const selectedTodoList = { id: '1', name: 'My List' };
  const wrapper = setup({ selectedTodoList });

  const newTodoItemName = 'To do item 1';
  const newTodoItemText = findByTestID(wrapper, 'new-item-text');
  newTodoItemText.simulate('changeText', newTodoItemName);
  expect(wrapper.state('itemName')).toEqual(newTodoItemName);
});

test('clicking new item button with populated new item textbox should add new item to the local state list, reflect on page, update database, and set textbox state to empty again', async () => {
  const newTodoItemName = 'To do item 1';
  const createdId = '11111';
  const selectedTodoList = { id: '1', name: 'My List' };
  const client = {
    mutate: jest.fn(() => Promise.resolve({
      data: {
        createToDoListItem: {
          id: createdId
        },
      },
    })),
  };

  const wrapper = setup({ selectedTodoList, client }, { itemName: newTodoItemName });

  const addNewTodoItemButton = findByTestID(wrapper, 'add-new-item-button');
  addNewTodoItemButton.simulate('press');
  await wait(0);

  // when button is clicked it should call the db update
  expect(client.mutate).toHaveBeenCalledTimes(1);

  // when button is clicked it should update local state
  expect(wrapper.state('todoListItems')[0].name).toEqual(newTodoItemName);
  expect(wrapper.state('todoListItems')[0].id).toEqual(createdId);

  // when button is clicked it should reset textbox state
  expect(wrapper.state('itemName')).toEqual('');

  // should render todo-list-items component
  const todoListItemsComponent = findByTestID(wrapper, 'todo-list-items');
  expect(todoListItemsComponent.length).toEqual(1);
});

test('load todo list items from db when component mounted', async () => {
  const todoListItemFromDb = {
    id: '1',
    name: 'my todo item',
    done: false
  };
  const client = {
    query: jest.fn(() => Promise.resolve({
      data: {
        ToDoList: {
          toDoListItems: [
            todoListItemFromDb
          ]
        },
      },
    })),
  };
  const selectedTodoList = { id: '1', name: 'My List' };
  const wrapper = setup({ selectedTodoList, client });
  await wait(0);
  // should call the db update function
  expect(client.query).toHaveBeenCalledTimes(1);
  expect(wrapper.state('todoListItems')).toEqual([todoListItemFromDb]);

  // should render todo-list-items component
  const todoListItemsComponent = findByTestID(wrapper, 'todo-list-items');
  expect(todoListItemsComponent.length).toEqual(1);
});

test('deleting a todo item should remove it from database and screen', async () => {
  const client = {
    mutate: jest.fn(() => Promise.resolve({})),
  };
  const todoListItems = [
    { id: '1', name: 'item 1', done: false },
    { id: '2', name: 'item 2', done: false },
    { id: '3', name: 'item 3', done: false },
  ];
  const selectedTodoList = { id: '1', name: 'My List' };
  const wrapper = setup({ selectedTodoList, client }, { todoListItems });

  const deleteItemButton = findByTestID(wrapper, 'todo-list-item-remove-button').first();
  expect(deleteItemButton.length).toEqual(1);
  deleteItemButton.simulate('press');
  await wait(0);
  wrapper.update();

  expect(client.mutate).toHaveBeenCalledTimes(1);
  expect(wrapper.state('todoListItems').length).toEqual(2);

  const secondItemName = findByTestID(wrapper, 'todo-list-item-name').first();
  expect(secondItemName.dive().text()).toEqual('item 2');
});

test('update a todo item should update the database and update the state', async () => {
  const client = {
    mutate: jest.fn(() => Promise.resolve({})),
  };
  const todoListItems = [
    { id: '1', name: 'item 1', done: false },
    { id: '2', name: 'item 2', done: false },
    { id: '3', name: 'item 3', done: false },
  ];
  const selectedTodoList = { id: '1', name: 'My List' };
  const wrapper = setup({ selectedTodoList, client }, { todoListItems });

  const checkboxItem = findByTestID(wrapper, 'todo-list-item-checkbox').first();
  checkboxItem.simulate('press');
  await wait(0);
  wrapper.update();

  expect(client.mutate).toHaveBeenCalledTimes(1);
  expect(wrapper.state('todoListItems')[0].done).toEqual(true);
});
