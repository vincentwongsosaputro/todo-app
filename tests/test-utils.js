function testID(id) {
  return cmp => cmp.props().testID === id;
}

/**
 * Return ShallowWrapper containing node(s) with the given data-test value.
 * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper.
 * @param {string} val - Value od data-test attribute for search.
 * @returns {ShallowWrapper}
 */
const findByTestID = (wrapper, val) => wrapper.findWhere(testID(val));

export { testID, findByTestID };
