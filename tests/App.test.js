import React from 'react';
import renderer from 'react-test-renderer';

import App from '../App';

describe('App', () => {
  it('renders without crashing after it load required data', () => {
    const rendered = renderer.create(<App />);
    const appComponent = rendered.root.instance;
    appComponent.setState({ isReady: true });

    expect(rendered).toBeTruthy();
  });
});
