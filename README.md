# ToDo-App

Basic Todo App built using react native and graphql.

<img src="images/1.png" width="225" /> 
<img src="images/2.png" width="225" /> 
<img src="images/3.png" width="225" />

## Demo

Try the app on expo: [todo-app](https://expo.io/@vincentwongso/todo-app)

## Setup

### 1. Clone the repo

```sh
git clone https://gitlab.com/vincentwongsosaputro/todo-app.git
cd todo-app
```

### 2. Setup Graphcool Service

I used [Graphcool](https://www.graph.cool/) as graphql backend as a service.
To setup the graphql database using graphcool, simply install graphcool cli by:

```sh
yarn global add graphcool
```

Then run:

```sh
graphcool init .
graphcool deploy
```

Save the HTTP endpoint for the `Simple API` from the output, you'll need it in the next step.

### 3. Connect the app with your GraphQL API

Paste the `Simple API` endpoint to `./App.js` as the `uri` argument in the `createNetworkInterface` call:

```js
// replace uri below(__SIMPLE_API_ENDPOINT__) with the endpoint from the previous step
const httpLink = new HttpLink({ uri: '__SIMPLE_API_ENDPOINT__' });
```

### 4. Install Dependencies & run locally

```sh
cd todo-app
yarn install
yarn start          # open using the Expo app on your phone
# yarn run ios      # open with iOS simulator
# yarn run android  # open with Android emulator
```

## Testing

The project currently contains unit test under `./tests/` directory. In order to execute these tests, run `$ yarn test`.

## Future Work

- [ ] Graphql Integration Tests
- [ ] End to end testing with Detox
- [ ] Clean Up UI
- [ ] Swipeable delete for removing list and todo item
- [ ] Confirmation before removing a list
